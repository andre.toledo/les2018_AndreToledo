<html>

<head>
  <title>Criar Topico</title>
  <link rel="stylesheet" type="text/css" href="cssCriaTopico.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>
</head>

<body>
  <!--div tudo-->
  <div class="tudo">

    <!--div barra logo-->
    <div class="barra-logo-botoes">

      <!--Logo-imagem-->
      <div class="logo">

        <a href="index.php">
          <img src='log.png'/ id="logo" title="Home">
        </a>
      </div>
    </div>

    <div class="criatopico">
      <!--areaCriaTopico-->
      <div class="areacriatopico">

        <!--div titulo-->
        <div class="labelcriatopico">
          <button type="button" id="buttoncriatopico" disabled>Criar Topico</button>
        </div>

        <!--label-->
        <div class="label">

          <button type="button" id="labelbutton" disabled>Titulo do Topico</button></br>
          <button type="button" id="labelbutton" disabled>Descrição</button>

        </div>
        <!--div form-->
        <div class="formtopico">

          <form action="AutenticaLogin.php" method="POST">
            <p>
              <input type="text" name="titulo" placeholder="Titulo do Topico">
            </p>
            <textarea rows="15" cols="40" name="descricao" form="formtopico">Coloque a Descrição aqui....
          </textarea>
        </div>

        <!--div botao voltar-->
        <div class="BarraBotoesVoltar">

          <button type="submit" id="Criar" title=Criar">Criar</button></br>
          </form>

          <a href="index.php"><button type="button" id="volta" title="Voltar">Voltar</button></a>
        </div>
      </div>
    </div>
  </div>
</body>

</html>