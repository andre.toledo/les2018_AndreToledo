<?php 
	require_once 'ConexaoBD.php';
	class FuncoesTopico{
		private $id;
		private $idtopico;
		
		public function getid(){
			return $this->id;
		}
		public function setid($id){
			$this ->id=$id;
		}
		public function getidtopico(){
			return $this->idtopico;
		}
		public function setidtopico($idtopico){
			$this->idtopico=$idtopico;
		}

		public function buscarTodos(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from meustopicos order by idtopico"
				);
				$stmt -> execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"insert into meustopicos(idtopico) values(:t)"
				);
				$stmt->bindValue(":t",$this->gettitulotopico());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function excluir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"delete from meustopicos where idtopico=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from meustopicos where idtopico=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>