<html>

<head>
  <title>Meus Topicos</title>
  <link rel="stylesheet" type="text/css" href="cssMeusTopicos.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>
</head>

<body>
  <!--div tudo-->
  <div class="tudo">

    <!--div barra logo-->
    <div class="barra-logo-botoes">

      <!--Logo-imagem-->
      <div class="logo">

        <a href="index.php">
          <img src='log.png'/ id="logo" title="Home">
        </a>
      </div>
      <div class="botoes">
        <button type="button" id="criatopico" title="Criar Topico"><a href="CriaTopico.php">Criar Topico</a></button>&nbsp;
        <button type="button" id="meustopico" title="Meus Topicos"><a href="MeusTopicos.php">Meus Topicos</a></button>&nbsp;
        <button type="button" id="login" title="Login/Cadastro"><a href="login.php">Login/Cadastro</a></button>
      </div>
    </div>

    <!--areaCriaTopico-->
    <div class="areameustopicos">

      <!--div titulo-->
      <div class="labelmeustopicos">
        <button type="button" id="buttonmeustopicos" disabled>Meus Topicos</button>
      </div>
      <div class="areatopicos">

      </div>
      <!--div botao voltar-->
      <div class="BarraBotoesVoltar">

        <a href="index.php"><button type="button" id="volta" title="Voltar">Voltar</button></a>
      </div>
    </div>
  </div>
</body>

</html>