<?php 
	require_once 'ConexaoBD.php';
	class FuncoesTopico{
		private $id;
		private $nome;
		private $senha;
		private $email;
		private $apelido;
		
		public function getid(){
			return $this->id;
		}
		public function setid($id){
			$this ->id=$id;
		}
		public function getnome(){
			return $this->nome;
		}
		public function setnome($nome){
			$this->nome=$nome;
		}
		public function getsenha(){
			return $this->senha;
		}
		public function setsenha($senha){
			$this->senha=$senha;
		}
		public function getemail(){
			return $this->email;
		}
		public function setemail($email){
			$this->email=$email;
		}
		public function getapelido(){
			return $this->apelido;
		}
		public function setapelido($apelido){
			$this->apelido=$apelido;
		}


		public function buscarTodos(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from usuario order by nome"
				);
				$stmt -> execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"insert into usuario(nome,senha,email,apelido) values(:n,:s,:e,:a)"
				);
				$stmt->bindValue(":n",$this->getnome());
				$stmt->bindValue(":s",$this->getsenha());
				$stmt->bindValue(":e",$this->getemail());
				$stmt->bindValue(":a",$this->getapelido());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function excluir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"delete from usuario where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from usuario where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>