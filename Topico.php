<html>

<head>
  <title>Topico</title>
  <link rel="stylesheet" type="text/css" href="cssTopico.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>
</head>

<body>
  <!--div tudo-->
  <div class="tudo">

    <!--div barra logo-->
    <div class="barra-logo-botoes">

      <!--Logo-imagem-->
      <div class="logo">

        <a href="index.php">
          <img src='log.png'/ id="logo" title="Home">
        </a>
      </div>

      <!--div opcao e login-->
      <div class="botoes">
        <button type="button" id="criatopico" title="Criar Topico"><a href="CriaTopico.php">Criar Topico</a></button>&nbsp;
        <button type="button" id="meustopico" title="Meus Topicos"><a href="MeusTopicos.php">Meus Topicos</a></button>&nbsp;
        <button type="button" id="login" title="Login/Cadastro"><a href="login.php">Login/Cadastro</a></button>
      </div>

      <!--div edita e volta-->
      <div class="edita-volta">

        <a href="EditaTopico.php"><button type="button" id="editar" title="editar">Editar</button></a>
        <a href="index.php"><button type="button" id="voltar" title="voltar">voltar</button></a>
      </div>
    </div>

    <!--areaTopico-->
    <div class="areatopico">

      <div class="topico">

        <!--div titulo-->
        <div class="labeltopico">
          <button type="button" id="buttontopico" disabled>Topico</button>
        </div>

        <!--label-->
        <div class="label">

          <button type="button" id="labelbutton" disabled>Titulo do Topico</button></br>
          <button type="button" id="labelbutton" disabled>Descrição</button>
        </div>

        <!--div form-->
        <div class="formtopico">

          <form action="AutenticaLogin.php" method="POST">
            <p>
              <input type="text" name="titulo" placeholder="Titulo do Topico">
            </p>
            <textarea rows="10" cols="95" name="descricao" form="formtopico">Coloque a Descrição aqui....
          </textarea>
          </form>
        </div>

        <div class="linha">
        </div>

        <!--div comentarios-->
        <div class="comentarios">

          <p>
            comentarios aqui
          </p>

        </div>

        <!--div comentar-->
        <div class="comentar">

          <!--label comentar-->
          <div class="labelcomentario">
            <button type="button" id="buttoncomentario" disabled>Comentar</button></br>
          </div>

          <!--input comentario-->
          <div class="inputcomentario">
            <form action="AutenticaLogin.php" method="POST">
              <p>
                <input type="text" name="comentario" placeholder="Comente Aqui">
              </p>
          </div>

          <!--subimit comentario-->
          <div class="labelenviar">

            <button type="submit" id="Enviar" title=Enviar">Enviar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>