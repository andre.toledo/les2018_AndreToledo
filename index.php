<html>

<head>
  <title>Home</title>
  <link rel="stylesheet" type="text/css" href="cssIndex.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>

</head>

<body>
  <div class="tudo">

    <div class="barra-logo-botao">

      <!--Logo-->
      <div class="logo">
        <a href="index.php">
          <img src='log.png'/ id="logo" title="Home">
        </a>
      </div>

      <!--botao-->
      <div class="botoes">
        <button type="button" id="criatopico" title="Criar Topico"><a href="CriaTopico.php">Criar Topico</a></button>&nbsp;
        <button type="button" id="meustopico" title="Meus Topicos"><a href="MeusTopicos.php">Meus Topicos</a></button>&nbsp;
        <button type="button" id="login" title="Login/Cadastro"><a href="login.php">Login/Cadastro</a></button>
      </div>

      <!--Barra pesquisa-->
      <div class="pesquisa">
        <p>
          Pesquisa: <input type="search" name="pesquisa" placeholder="Digite a aqui">
        </p>
      </div>
    </div>

    <div class="topico-chat">
      <!--Topicos Recentes-->

      <div class="TopicoRecente">
        <div class="labeltopico">
          <button type="button" id="buttontopico" disabled>Topicos</button>
        </div>
        <div class="areatopico">
          <?php
			require_once 'FuncoesTopico.php';
			$c = new Funcoestopico();
			$dados=$c->buscarTodos();
            echo "<table class='topicotable'>";
			echo "<tr><th></th><th></th></tr>";
			foreach($dados as $linha){
                print "<tr>";
                print "<td>".$linha[1]."</td>";
				print "<td>".$linha[2]."</td>";
                print "</tr>";
				}
			echo "</table>";
		?>
          <?php
			if(isset($_POST['titulo'])){
				$c->settitulo($_POST['titulo']);
				$c->setdescricao($_POST['descricao']);
				$c->setcomentario($_POST['comentario']);
				$c->inserir();
				unset($_POST['titulo']);
				header("Refresh:0");
			}
		
		?>
        </div>
      </div>

      <!--Chat-->
      <div class="chat">
        <div class="labelchat">
          <button type="button" id="buttonchat" disabled>Chat</button>
        </div>

        <div class="areachat">
        </div>
      </div>

    </div>
  </div>
</body>

</html>