<html>

<head>
  <title>Login</title>
  <link rel="stylesheet" type="text/css" href="cssLogin.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>

</head>

<body>
  <div class="tudo">

    <div class="barra-logo-botao">

      <!--Logo-->
      <div class="logo">
        <a href="index.php">
          <img src='log.png'/ id="logo" title="Home">
        </a>
      </div>
    </div>

    <div class="logincadastro">

      <!--LoginCadastro-->
      <div class="arealogincadastro">

        <div class="labellogin">
          <button type="button" id="buttonlogin" disabled>Login</button>
        </div>

        <div class="label">
          <button type="button" id="labels" disabled>User Name</button></br>
          <button type="button" id="labels" disabled>Senha</button>
        </div>

        <div class="formlogin">

          <form action="AutenticaLogin.php" method="POST">
            <p>
              <input type="text" name="nome" placeholder="Digite o user name aqui">
            </p>
            <p>
              <input type="password" name="senha" placeholder="Digite a senha aqui">
            </p>

        </div>

        <div class="subimit">
          <button type="submit" id="entrar" title="Entrar">Entrar</button>
          </form>
        </div>

        <div class="linha">
        </div>

        <div class="areaCadastro">
          <button type="button" id="mensagemcadastro" disabled>Não tem uma conta? Cadastre-se agora!!</button></br>
          <a href="CriaCadastro.php"><button type="button" id="cadastro" title="Criar Cadastro">Cadastrar-se</button></a></br>
          <a href="index.php"><button type="button" id="volta" title="Voltar">Voltar</button></a>
        </div>

      </div>
    </div>
  </div>
</body>

</html>