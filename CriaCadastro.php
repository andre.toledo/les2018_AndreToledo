<html>

<head>
  <title>Criar Cadastro</title>
  <link rel="stylesheet" type="text/css" href="cssCadastro.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous">
  </script>

</head>

<body>
  <div class="tudo">
    <div class="barra-logo-botao">

      <!--Logo-->
      <div class="logo">
        <a href="index.php">
          <img src='log.png'/ id="logo" title="Home">
        </a>
      </div>
    </div>

    <!--Cadastro-->
    <div class="Cadastro">

      <div class="areacadastro">
        <div class="labelcadastro">
          <button type="button" id="buttonCadastro" disabled>Cadastro</button>
        </div>

        <div class="label">

          <button type="button" id="buttonlabel" disabled>Nome</button></br>
          <button type="button" id="buttonlabel" disabled>Senha</button></br>
          <button type="button" id="buttonlabel" disabled>Email</button></br>
          <button type="button" id="buttonlabel" disabled>Apelido no forum</button>
        </div>

        <div class="formcadastro">

          <form method="POST">

            <p>
              <input type="text" name="nome" placeholder="Digite seu nome aqui" required>
            </p>
            <p>
              <input type="password" name="senha" placeholder="Digite a senha aqui" required>
            </p>
            <p>
              <input type="text" name="email" placeholder="Digite seu email aqui">

            </p>
            <p>
              <input type="text" name="apelido" placeholder="Digite o apelido aqui" required>

            </p>
        </div>
        <div class="barrabotoesvoltar">

          <input id="criaCadastro" type="submit" value="Cadastrar" title="Cadastrar"></br>
          </form>
          <a href="index.php"><button type="button" id="volta" title="Voltar">Voltar</button></a>

        </div>
      </div>
    </div>
  </div>
</body>

</html>