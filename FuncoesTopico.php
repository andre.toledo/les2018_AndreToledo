<?php 
	require_once 'ConexaoBD.php';
	class FuncoesTopico{
		private $id;
		private $titulo;
		private $descricao;
		private $comentario;
		private $idcriador;
		
		public function getid(){
			return $this->id;
		}
		public function setid($id){
			$this ->id=$id;
		}
		public function gettitulo(){
			return $this->titulo;
		}
		public function settitulo($titulo){
			$this->titulo=$titulo;
		}
		public function getdescricao(){
			return $this->descricao;
		}
		public function setdescricao($descricao){
			$this->descricao=$descricao;
		}
		public function getcomentario(){
			return $this->comentario;
		}
		public function setcomentario($comentario){
			$this->comentario=$comentario;
		}
		public function getidcriador(){
			return $this->idcriador;
		}
		public function setidcriador($idcriador){
			$this->idcriador=$idcriador;
		}

		public function buscarTodos(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from topico order by titulo"
				);
				$stmt -> execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		public function inserir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"insert into topico(titulo,descricao,comentario,idcriador) values(:n,:d,:c,:ic)"
				);
				$stmt->bindValue(":n",$this->gettitulo());
				$stmt->bindValue(":d",$this->getdescricao());
				$stmt->bindValue(":c",$this->getcomentario());
				$stmt->bindValue(":ic",$this->getidcriador());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function excluir(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"delete from topico where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from topico where id=:i"
				);
				$stmt->bindValue(":i",$this->getid());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarIdCriador(){
			$c=new ConexaoBD();
			try{
				$stmt=$c->conn->prepare(
					"select * from topico where idCriador=:ic"
				);
				$stmt->bindValue(":ic",$this->getidCriado());
				$stmt -> execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
	}
?>